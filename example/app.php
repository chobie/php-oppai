<?php
$start = microtime(true);

class Router{
    /**
     * 配列でクラス名とメソッド名返せばKernelが実行してくれるよ
     */
    public function dispatch()
    {
        return array("Blog","moeAction");
    }
}

class Blog extends Oppai\Controller
{
    public function moeAction()
    {
        echo "Hello Moe";
    }
    
    // きっとそのうちResponsオブジェクト返せとかいうと思う
    public function indexAction()
    {
        echo "<html><body>Hello World</body></html>";
    }
}

class ApplicationOppai extends Oppai\Kernel
{
    public function registerConfiguration($loader)
    {
        //めんどいのでOppai\Containerを直接渡している
        //本当は一段かましてLoaderとかでやりたい
        $loader->set("routing",new Router());
    }
}

$oppai = new ApplicationOppai("develop");
$result = $oppai->run();
$end = microtime(true);
printf("start: %s\n",$start);
printf("end: %s\n",$end);
