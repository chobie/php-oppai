<?php
$fp = fopen("container.php","r");

$container = new Oppai\Container();
$container->set("key","Hello");
$container->set("fp",$fp);
$container->set("container",1);
$container->set("double",1.1);
$container->set("bool",true);
$container->set("bool2",false);

var_dump($container->get("key"));
var_dump($container->get("fp"));
var_dump($container->get("container"));
var_dump($container->get("double"));
var_dump($container->get("bool"));
var_dump($container->get("bool2"));