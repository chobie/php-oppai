/*
 * The MIT License
 *
 * Copyright (c) 2010 - 2011 Shuhei Tanuma
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "php_oppai.h"

PHPAPI zend_class_entry *oppai_class_entry;

extern void oppai_framework_container_init(TSRMLS_D);
extern void oppai_framework_kernel_init(TSRMLS_D);
extern void oppai_framework_controller_init(TSRMLS_D);

void oppai_init(TSRMLS_D);
PHP_MINIT_FUNCTION(oppai);
PHP_MINFO_FUNCTION(oppai);

PHPAPI function_entry php_oppai_methods[] = {
    {NULL, NULL, NULL}
};

PHP_MINIT_FUNCTION(oppai) {
    oppai_framework_container_init(TSRMLS_C);
    oppai_framework_kernel_init(TSRMLS_C);
    oppai_framework_controller_init(TSRMLS_C);
    return SUCCESS;
}

PHP_MINFO_FUNCTION(oppai)
{
    php_printf("PHP Oppai Framework Extension\n");
    php_info_print_table_start();
    php_info_print_table_row(2,"Version", PHP_OPPAI_EXTVER " (eternal alpha development)");
    php_info_print_table_row(2, "Authors", "Shuhei Tanuma 'stanuma@zynga.co.jp' (lead)\n");
    php_info_print_table_end();
}

zend_module_entry oppai_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
    STANDARD_MODULE_HEADER,
#endif
    PHP_OPPAI_EXTNAME,
    NULL,	/* Functions */
    PHP_MINIT(oppai),	/* MINIT */
    NULL, 	/* MSHUTDOWN */
    NULL, 	/* RINIT */
    NULL,	/* RSHUTDOWN */
    PHP_MINFO(oppai),	/* MINFO */
#if ZEND_MODULE_API_NO >= 20010901
    PHP_OPPAI_EXTVER,
#endif
    STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_OPPAI
ZEND_GET_MODULE(oppai)
#endif