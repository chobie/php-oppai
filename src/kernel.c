/*
 * The MIT License
 *
 * Copyright (c) 2010 - 2011 Shuhei Tanuma
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "php_oppai.h"

PHPAPI zend_class_entry *oppai_framework_kernel_class_entry;
void oppai_framework_kernel_init(TSRMLS_D);
static void oppai_framework_kernel_free_storage(oppai_framework_kernel *obj TSRMLS_DC);
zend_object_value oppai_framework_kernel_new(zend_class_entry *ce TSRMLS_DC);
PHP_METHOD(oppai_kernel, __construct);
PHP_METHOD(oppai_kernel, getContainer);
PHP_METHOD(oppai_kernel, getEnvironment);
PHP_METHOD(oppai_kernel, boot);
PHP_METHOD(oppai_kernel, run);
int oppai_framework_kernel_bootstrap(oppai_framework_kernel *kernel, zval *class);


int oppai_framework_kernel_bootstrap(oppai_framework_kernel *kernel, zval *class TSRMLS_DC) 
{
    zval *ret;
    zval f;

    MAKE_STD_ZVAL(ret);
    ZVAL_NULL(ret);
    ZVAL_STRING(&f,"boot", 1);
    call_user_function(NULL,&class,&f,ret,0,NULL TSRMLS_CC);
    zval_ptr_dtor(&ret);
    zval_dtor(&f);

    return 1;
}


ZEND_BEGIN_ARG_INFO_EX(arginfo_oppai_framework_kernel__construct, 0, 0, 1)
    ZEND_ARG_INFO(0, environment)
ZEND_END_ARG_INFO()


static void oppai_framework_kernel_free_storage(oppai_framework_kernel *obj TSRMLS_DC)
{
    zend_object_std_dtor(&obj->zo TSRMLS_CC);
    if(obj->environment != NULL) {
        efree(obj->environment);
        obj->environment = NULL;
    }
    efree(obj);
}

zend_object_value oppai_framework_kernel_new(zend_class_entry *ce TSRMLS_DC)
{
    zend_object_value retval;
    oppai_framework_kernel *obj;
    zval *tmp;

    obj = ecalloc(1, sizeof(*obj));
    zend_object_std_init( &obj->zo, ce TSRMLS_CC );
    zend_hash_copy(obj->zo.properties, &ce->default_properties, (copy_ctor_func_t) zval_add_ref, (void *) &tmp, sizeof(zval *));

    retval.handle = zend_objects_store_put(obj, 
        (zend_objects_store_dtor_t)zend_objects_destroy_object,
        (zend_objects_free_object_storage_t)oppai_framework_kernel_free_storage,
    NULL TSRMLS_CC);
    retval.handlers = zend_get_std_object_handlers();
    return retval;
}

PHP_METHOD(oppai_kernel, __construct){
    oppai_framework_kernel *this = (oppai_framework_kernel *) zend_object_store_get_object(getThis() TSRMLS_CC);
    char *environment = "develop";
    int  environment_len;
    
    if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC,
        "|s",&environment, &environment_len) == FAILURE){
        return;
    }
    
    this->booted      = 0;
    this->environment = estrdup(environment);
}

PHP_METHOD(oppai_kernel, getEnvironment){
    oppai_framework_kernel *this = (oppai_framework_kernel *) zend_object_store_get_object(getThis() TSRMLS_CC);
    RETURN_STRING(this->environment,1);
}

PHP_METHOD(oppai_kernel, boot){
    oppai_framework_kernel *this = (oppai_framework_kernel *) zend_object_store_get_object(getThis() TSRMLS_CC);
    zval *container;
    
    MAKE_STD_ZVAL(container);
    object_init_ex(container,oppai_framework_container_class_entry);
    this->container = container;

    zval *retval;
    zval func;
    zval *params[1];
    MAKE_STD_ZVAL(params[0]);

    // Todo: LoaderInterfaceとかでよませたいけど面倒なのでコンテナを直接渡す
    params[0] = container;
    MAKE_STD_ZVAL(retval);
    ZVAL_NULL(retval);
    ZVAL_STRING(&func,"registerConfiguration", 1);
    call_user_function(NULL,&getThis(),&func,retval,1,params TSRMLS_CC);
    //Todo 返されたvalueを元にconfigを設定する

    zval_ptr_dtor(&retval);
    zval_dtor(&func);

    this->booted = 1;
}

PHP_METHOD(oppai_kernel, run){
    oppai_framework_kernel *this = (oppai_framework_kernel *) zend_object_store_get_object(getThis() TSRMLS_CC);

    if(this->booted != OPPAI_BOOTED) {
        this->booted = oppai_framework_kernel_bootstrap(this,getThis() TSRMLS_CC);
    }
    
    zval *router;
    zval getter;
    zval *params[1];
    zval param;
    MAKE_STD_ZVAL(params[0]);
    ZVAL_STRING(&param,"routing",1);
    // Todo: LoaderInterfaceとかでよませたいけど面倒なのでコンテナを直接渡す
    params[0] = &param;

    MAKE_STD_ZVAL(router);
    ZVAL_NULL(router);
    ZVAL_STRING(&getter,"get",1);
    call_user_function(NULL,&this->container,&getter,router,1,params TSRMLS_CC);

    //Routingを呼ぶ
    zval dispatch;
    zval *actions;
    MAKE_STD_ZVAL(actions);
    ZVAL_NULL(actions);
    ZVAL_STRING(&dispatch,"dispatch",1);
    
    call_user_function(NULL,&router,&dispatch,actions,0,NULL TSRMLS_CC);
    if(Z_TYPE_P(actions) == IS_ARRAY){
        //fprintf(stderr,"ARRAY");
    }
    //Class名とAction名をとる
    char *key;
    zval **data;
    ulong idx;
    uint keylen;
    HashTable *array_hash;
    HashPosition pointer;
    array_hash = Z_ARRVAL_P(actions);
    int array_count = 0;

    array_count = zend_hash_num_elements(array_hash);
    
    zend_hash_internal_pointer_reset_ex(array_hash,&pointer);
    zend_hash_get_current_key_ex(array_hash,&key,&keylen,&idx,0,&pointer);
    zend_hash_get_current_data_ex(array_hash, (void **)&data, &pointer);

    char *controllerName = Z_STRVAL_PP(data);
    zval **zaction;
    zend_hash_move_forward_ex(array_hash, &pointer);
    zend_hash_get_current_data_ex(array_hash, (void **)&zaction, &pointer);
    char *actionName = Z_STRVAL_PP(zaction);

    zend_class_entry **ce;
    zval _controller;
    zval *controller = &_controller;

    //TODO: 変更できるようにする
    if (zend_lookup_class(controllerName, strlen(controllerName), &ce TSRMLS_CC) == FAILURE) {
        fprintf(stderr,"%s not found",controllerName);
        return;
    }

    object_init_ex(controller,*ce);
    zval *retval;
    zval func;

    MAKE_STD_ZVAL(retval);
    ZVAL_NULL(retval);
    ZVAL_STRING(&func,actionName, 1);
    call_user_function(NULL,&controller,&func,retval,0,NULL TSRMLS_CC);
    zval_ptr_dtor(&retval);
    zval_dtor(&func);
    zval_ptr_dtor(&controller);

    zval_ptr_dtor(&actions);
    zval_dtor(&dispatch);
    zval_ptr_dtor(&router);
    zval_dtor(&getter);
    

}

PHPAPI function_entry oppai_framework_kernel_methods[] = {
    PHP_ME(oppai_kernel,               __construct,      arginfo_oppai_framework_kernel__construct, ZEND_ACC_PUBLIC)
    PHP_ABSTRACT_ME(oppai_kernel,      registerConfiguration,     NULL)
    PHP_ME(oppai_kernel,               getEnvironment,   NULL, ZEND_ACC_PUBLIC)
    PHP_ME(oppai_kernel,               boot,             NULL, ZEND_ACC_PUBLIC)
    PHP_ME(oppai_kernel,               run,              NULL, ZEND_ACC_PUBLIC)
    {NULL, NULL, NULL}
};

void oppai_framework_kernel_init(TSRMLS_D)
{
    zend_class_entry ce;
    INIT_NS_CLASS_ENTRY(ce, PHP_OPPAI_NS,"Kernel", oppai_framework_kernel_methods);
    oppai_framework_kernel_class_entry = zend_register_internal_class(&ce TSRMLS_CC);
    oppai_framework_kernel_class_entry->ce_flags |= ZEND_ACC_IMPLICIT_ABSTRACT_CLASS;
    oppai_framework_kernel_class_entry->create_object = oppai_framework_kernel_new;
}