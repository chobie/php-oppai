PHP_ARG_ENABLE(oppai,Whether to enable the "oppai" extension,
  [  --enable-oppai      Enable "php-oppai" extension support])

if test $PHP_OPPAI != "no"; then
  PHP_NEW_EXTENSION(oppai, oppai.c container.c kernel.c controller.c, $ext_shared)

  ifdef([PHP_ADD_EXTENSION_DEP],
  [
    PHP_ADD_EXTENSION_DEP(oppai, spl, true)
  ])

fi