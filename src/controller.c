/*
 * The MIT License
 *
 * Copyright (c) 2010 - 2011 Shuhei Tanuma
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "php_oppai.h"

PHPAPI zend_class_entry *oppai_framework_controller_class_entry;
void oppai_framework_controller_init(TSRMLS_D);
static void oppai_framework_controller_free_storage(oppai_framework_controller *obj TSRMLS_DC);
zend_object_value oppai_framework_controller_new(zend_class_entry *ce TSRMLS_DC);

static void oppai_framework_controller_free_storage(oppai_framework_controller *obj TSRMLS_DC)
{
    zend_object_std_dtor(&obj->zo TSRMLS_CC);
    efree(obj);
}

zend_object_value oppai_framework_controller_new(zend_class_entry *ce TSRMLS_DC)
{
    zend_object_value retval;
    oppai_framework_controller *obj;
    zval *tmp;

    obj = ecalloc(1, sizeof(*obj));
    zend_object_std_init( &obj->zo, ce TSRMLS_CC );
    zend_hash_copy(obj->zo.properties, &ce->default_properties, (copy_ctor_func_t) zval_add_ref, (void *) &tmp, sizeof(zval *));

    retval.handle = zend_objects_store_put(obj, 
        (zend_objects_store_dtor_t)zend_objects_destroy_object,
        (zend_objects_free_object_storage_t)oppai_framework_controller_free_storage,
    NULL TSRMLS_CC);
    retval.handlers = zend_get_std_object_handlers();
    return retval;
}

PHPAPI function_entry oppai_framework_controller_methods[] = {
    {NULL, NULL, NULL}
};

void oppai_framework_controller_init(TSRMLS_D)
{
    zend_class_entry ce;
    INIT_NS_CLASS_ENTRY(ce, PHP_OPPAI_NS,"Controller", oppai_framework_controller_methods);
    oppai_framework_controller_class_entry = zend_register_internal_class(&ce TSRMLS_CC);
    oppai_framework_controller_class_entry->ce_flags |= ZEND_ACC_IMPLICIT_ABSTRACT_CLASS;
    oppai_framework_controller_class_entry->create_object = oppai_framework_controller_new;
}