/*
 * The MIT License
 *
 * Copyright (c) 2010 - 2011 Shuhei Tanuma
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PHP_OPPAI_H

#define PHP_OPPAI_H

#define PHP_OPPAI_EXTNAME "oppai"
#define PHP_OPPAI_EXTVER "0.1"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include <spl/spl_array.h>
#include <zend_interfaces.h>
#include <string.h>
#include <time.h>

/* Define the entry point symbol
 * Zend will use when loading this module
 */
extern zend_module_entry oppai_module_entry;
#define phpext_oppai_ptr &oppai_module_entry;
#define PHP_OPPAI_NS "Oppai"

#define OPPAI_BOOTED 1

extern PHPAPI zend_class_entry *oppai_framework_kernel_class_entry;
extern PHPAPI zend_class_entry *oppai_framework_controller_class_entry;
extern PHPAPI zend_class_entry *oppai_framework_container_class_entry;

typedef struct{
    zend_object zo;
}oppai_framework_container;

typedef struct{
    zend_object zo;
    char *environment;
    short booted;
    zval *container;
}oppai_framework_kernel;

typedef struct{
    zend_object zo;
}oppai_framework_controller;

#endif /* PHP_OPPAI_H */